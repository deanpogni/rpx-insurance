### Theme
The RPX Insurance theme uses the Underscore (`_s`) Wordpress base theme with `_sassify`.

## Local development with MAMP
Info on setting up MAMP vhosts: http://foundationphp.com/tutorials/vhosts_mamp.php

Local dev URL: `rpx-insurance.dev`

## Current development TODO items (to be done this week)
[ ] Finalize Mobile navigation
[ ] Overall mobile styles
[x] Get a quote form
[x] contact us form
[x] Icons
[x] Footer
[x] Clean up Broker Rundown template
[x] Mobile nav
[x] General style clean up
[X] Contact us page
[x] Case studies page
[x] Headline background images

## Items for launch
[ ] Design approval from RPX and Laura
[x] Input final content
[ ] confirm RPX user accounts
[ ] Analytics embed codes
  [?] Add Google Analytics tracking code --- I added GTM
  [x] Add LeadLander tracking (below)
[x] Confirm form submits
  [x] in QA
  [ ] in Prod
[ ] integrate into RPX multisite.
  [ ] QA
  [ ] Prod
[ ] Configure DNS with RPX's IT team
[ ] Browser testing
[ ] Clean out rpxcorp.com site. Direct relevant links to rpxinsurance.com

### LeadLander tracking code

```<script type="text/javascript" language="javascript">
var tl813v = 14014;
(function() {
var tl813 = document.createElement('script'); tl813.type = 'text/javascript'; tl813.async = true;
tl813.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '1.tl813.com/tl813.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tl813, s);
})();
</script>```
