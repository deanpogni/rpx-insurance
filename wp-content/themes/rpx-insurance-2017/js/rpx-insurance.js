/*********************
Case Studies Click Toggle
*********************/

var caseStudies = document.querySelectorAll(".case-study-results__result");

for(i = 0; i < caseStudies.length; i++) {
    caseStudies[i].addEventListener('click', function(event){

        var readMoreBtn = this.querySelector('.case-study-results__read-more-btn');
        if ( event.target == readMoreBtn ) {

            event.preventDefault();

            this.querySelector('.case-study-results__read-more').classList.toggle('case-study-results__read-more--visible');

            if ( readMoreBtn.getAttribute('data-link') == 'more' ) {
                readMoreBtn.setAttribute('data-link', 'less');
                readMoreBtn.innerHTML = "+ Read more";
            } else {
                readMoreBtn.setAttribute('data-link', 'more');
                readMoreBtn.innerHTML = "&ndash; Read less";
            }
        }
    });
}

/*********************
FAQ Click Toggle
*********************/

var faqItems = document.querySelectorAll(".faq__item");

for(i = 0; i < faqItems.length; i++) {
    faqItems[i].addEventListener('click', function(event) {
        event.preventDefault();
        if ( event.target.classList.contains('faq__headline') && this.querySelector('.faq__text') ) {
            this.querySelector('.faq__text').classList.toggle('faq__text--visible');
        }
    });
}

$(function() {
    /**
    * Match Heights
    **/
	$(".box").matchHeight({
        byRow: false
    });
    $(".news-articles__item").matchHeight({
        byRow: false
    });
    $(".press-releases__item").matchHeight({
        byRow: false
    });
    $(".insurance-tool__link").matchHeight({
        byRow: false
    });

    /**
    * Footer empty anchor tags
    **/
    $(".site-footer a[href*='#']").click(function(event) {
        event.preventDefault();
    });

    /**
    * All PDFs links to open in new window
    **/
    $('a[href$=".pdf"]').attr('target', '_blank');
    /**
    * Validate forms with jquery.validiator.js
    **/
    $('.input-form').validate();

});
