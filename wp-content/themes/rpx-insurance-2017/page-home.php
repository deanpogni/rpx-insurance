<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

get_header(); ?>

<div class="home__headline-wrap">
	<h1 class="home__headline"><?php echo get_field('headline'); ?></h1>
</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="text--subheadline">
				<?php echo get_field('subheadline'); ?>
			</div>

			<?php

			// check if the repeater field has rows of data
			if( have_rows('get_a_quote_btn') ): ?>

			<div class="button-row">

			<?php	// loop through the rows of data
				while( have_rows('get_a_quote_btn') ): the_row();

				$btnText = get_sub_field('button_text');
				$btnLink = get_sub_field('button_link');

			?>

				<a href="<?php echo $btnLink ?>" class="button button--primary"><?php echo $btnText ?></a>

			<?php endwhile; ?>
			</div>
			<?php endif; ?><!-- .button-row -->

			<h2 class="title--extra-large text--center">Coverage for All Companies of All Sizes</h2>

			<?php if( have_rows('boxes') ): ?>

			<div class="box-wrapper">

				<?php
				// loop through the rows of data
				while( have_rows('boxes') ): the_row();

				// vars
				$headline = get_sub_field('headline');
				$text = get_sub_field('text');
				$image = get_sub_field('icon');
				$link = get_sub_field('link');
				?>

				<div class="box box--image-top">
					<a href="<?php echo $link; ?>" class="box__link">
						<?php if( $image ): ?>
							<div class="box__image-wrap box__image-wrap--top">
								<img src="<?php echo $image; ?>" alt="" />
							</div>
						<?php endif; ?>

						<div class="box__text-wrap">
							<?php if( $headline ): ?>
								<h4 class="box__headline"><?php echo $headline; ?></h4>
							<?php endif; ?>

							<?php if( $text ): ?>
								<p class="box__text"><?php echo $text; ?></p>
							<?php endif; ?>
						</div>
					</a>
				</div>

				<?php endwhile; ?>

			</div><!-- .faq__wrap -->

			<?php endif; ?>

			<div class="text--subheadline">
			<?php if ( get_field('text2') ): ?>
				<?php echo get_field('text2'); ?>
			<?php endif; ?>
			</div>

			<?php get_template_part( 'template-parts/learn-more' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
