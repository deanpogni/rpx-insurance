Author: Dean Pogni / dpogni@gmail.com
Date: April 2017

## Theme Info

This theme is built on top of the uses the Underscores starter theme `_s` aka Underscore.
For more info: http://underscores.me/

## Webpack & SASS

Webpack is used to compile the SCSS files inside src/sass. To run the compile,

- from the theme rolder, run `npm install`
- then run `npm run build`

For deveopment, run `npm run watch` for SASS watcher
