<?php
/**
 * rpx-insurance functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package rpx-insurance
 */

if ( ! function_exists( 'rpx_insurance_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rpx_insurance_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on rpx-insurance, use a find and replace
	 * to change 'rpx-insurance' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'rpx-insurance', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in several locations
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'rpx-insurance' ),
		'mobile-menu' => esc_html__( 'Mobile Menu', 'rpx-insurance' ),
		'menu-footer' => esc_html__( 'Footer', 'rpx-insurance' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'rpx_insurance_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'rpx_insurance_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function rpx_insurance_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'rpx_insurance_content_width', 640 );
}
add_action( 'after_setup_theme', 'rpx_insurance_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function rpx_insurance_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'rpx-insurance' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'rpx-insurance' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'rpx_insurance_widgets_init' );

/**
* Enqueue scripts and styles.
*/
function rpx_insurance_scripts() {

	wp_deregister_script('jquery');

	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);

	wp_enqueue_style( 'rpx-insurance-dist-style', get_stylesheet_uri() );

	wp_enqueue_script( 'rpx-insurance-custom', get_template_directory_uri() . '/js/rpx-insurance.js', array(), '', true );

	wp_enqueue_script( 'jquery.validate', get_template_directory_uri() . '/js/jquery.validate.js', array(), '1.16.1', true );

	wp_enqueue_script( 'jquery.matchHeight', get_template_directory_uri() . '/js/jquery.matchHeight.min.js', array(), '0.7.2', true );

	wp_enqueue_script( 'rpx-insurance-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'src', get_template_directory_uri() . '/src.js', array(), '20160401', true );

	wp_enqueue_script( 'rpx-insurance-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'rpx_insurance_scripts' );

class Nav_Footer_Walker extends Walker_Nav_Menu {
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		if (0 == $depth) {
			$output .= '<div class="sub-menu-wrapper"><a href="' . $item->url . '" class="sub-menu-top-link">' . $item->title . '</a>' ;
		} else {
			parent::start_el($output, $item, $depth, $args);
		}
    }
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		if (0 == $depth) {
			$output .= '</div>';
		} else {
			parent::end_el($output, $item, $depth, $args);
		}
	}
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Add Broker Rundown Post Type
 */
add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'broker-rundown',
	array(
		'labels' => array(
			'name' => __( 'Broker Rundown' ),
			'singular_name' => __( 'Broker Rundown' )
		),
		'public' => true,
		'has_archive' => true,
		)
	);
}

/**
 * disabled `Archives:` being prepended to archives page titles pages
 */
add_filter( 'get_the_archive_title', function ( $title ) {
	if( is_post_type_archive() ) {
		$title = sprintf( __( '%s' ), post_type_archive_title( '', false ) );
	}
	return $title;
});

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Remove 'posts' from admin sidebar
 */
function remove_menus(){
  remove_menu_page( 'edit.php' );
}
add_action( 'admin_menu', 'remove_menus' );

/**
 * Completely disable comments
 */

// Disable support for comments and trackbacks in post types
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'df_disable_comments_post_types_support');
// Close comments on the front-end
function df_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);
// Hide existing comments
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);
// Remove comments page in menu
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'df_disable_comments_admin_menu');
// Redirect any user trying to access comments page
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');
// Remove comments metabox from dashboard
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'df_disable_comments_dashboard');
// Remove comments links from admin bar
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'df_disable_comments_admin_bar');
/**
 * Remove anchor from archive's Read More links
 */
function remove_more_link_scroll( $link ) {
  $link = preg_replace( '|#more-[0-9]+|', '', $link );
  return $link;
}
add_filter( 'the_content_more_link', 'remove_more_link_scroll' );
