<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rpx-insurance
 */

?>

	</div><!-- #content -->

	<a href="http://www.rpxcorp.com/patent-risk-digest/" target="_blank" class="footer-cta">
		<span class="footer-cta-link">
			Read the latest Patent Risk Digest e-Newsletter
		</span>
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 width="37.5px" height="50.417px" viewBox="0 0 37.5 50.417" enable-background="new 0 0 37.5 50.417" xml:space="preserve">
			<g>
				<rect x="0.75" y="15" fill="none" stroke="#FFFFFF" stroke-width="1.5" stroke-miterlimit="10" width="36" height="34.667"/>
				<line fill="none" stroke="#FFFFFF" stroke-width="1.5" stroke-miterlimit="10" x1="18.755" y1="0" x2="18.755" y2="39.655"/>
				<polyline fill="none" stroke="#FFFFFF" stroke-width="1.5" stroke-miterlimit="10" points="8.953,24.348 18.75,39.655
					29.417,24.322 	"/>
			</g>
		</svg>
	</a>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-wrapper">
			<div class="footer-logo">
				<?php get_template_part( 'template-parts/rpx-logo' ); ?>
				<p class="copyright">&copy; 2008-<?php echo date("Y"); ?> RPX Corporation. All&nbsp;rights&nbsp;reserved.</p>
			</div>

			<?php wp_nav_menu(array(
				'theme_location'  => 'menu-footer',
				'container'       => false,
				'menu_id'         => 'footer-menu',
				'items_wrap'      => '<nav role="navigation" id="%1$s" class="%2$s">%3$s</nav>',
				'walker'          => new Nav_Footer_Walker()
			)); ?>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<!-- LeadLander Tracking -->
<script type="text/javascript" language="javascript">
	var tl813v = 14014;
	(function() {
	var tl813 = document.createElement('script'); tl813.type = 'text/javascript'; tl813.async = true;
	tl813.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '1.tl813.com/tl813.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tl813, s);
	})();
</script>

</body>
</html>
