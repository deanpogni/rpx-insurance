<?php
/**
 * The template for displaying the Broker Rundown index
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

get_header(); ?>

	<header class="page-header">
		<div class="page-header-wrap">
			<h1 class="page-title">The Broker Rundown</h1>
		</div>
	</header><!-- .page-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-broker-rundown', get_post_format() );

			endwhile;

		else :

			get_template_part( 'template-parts/content-broker-rundown', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
