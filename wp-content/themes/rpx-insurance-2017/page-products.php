<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

get_header(); ?>

	<header class="entry-header">
		<div class="page-header-wrap">

			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="100.001px" height="55.839px" viewBox="0 0 100.001 55.839" enable-background="new 0 0 100.001 55.839"
				 xml:space="preserve">
			<path fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" d="M50,1C22.938,1,1,25.104,1,54.839h28.378
				c1.295-5.977,4.611-11.071,9.118-14.316c2.916,3.297,6.989,5.354,11.504,5.354c4.516,0,8.588-2.057,11.504-5.354
				c4.508,3.245,7.822,8.34,9.117,14.316h28.379C99.001,25.104,77.063,1,50,1z M50,39.736c-5.777,0-10.46-5.146-10.46-11.493
				S44.223,16.749,50,16.749c5.778,0,10.461,5.146,10.461,11.494S55.778,39.736,50,39.736z"/>
			</svg>


			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<h2 class="title--extra-large text--center"><?php echo get_field('top_headline'); ?></h2>

			<div class="text--subheadline text--subheadline--products"><?php echo get_field('text'); ?></div>


			<?php

			// check if the repeater field has rows of data
			if( have_rows('get_a_quote_btn') ): ?>

			<div class="button-row">

			<?php	// loop through the rows of data
				while( have_rows('get_a_quote_btn') ): the_row();

				$btnText = get_sub_field('button_text');
				$btnLink = get_sub_field('button_link');

			?>

				<a href="<?php echo $btnLink ?>" class="button button--primary"><?php echo $btnText ?></a>

			<?php endwhile; ?>
			</div>
			<?php endif; ?>

			<?php

			// check if the repeater field has rows of data
			if( have_rows('product_suite') ): ?>

			<h2 class="title--extra-large text--center"><?php echo the_field('product_suite_headline'); ?></h2>

			<div class="product-suite-boxes">

				<?php	// loop through the rows of data
					while( have_rows('product_suite') ): the_row();

					// vars
					$headline = get_sub_field('headline');
					$text = get_sub_field('text');

				?>
				<div class="product-suite-box">

					<?php if( $headline ): ?>
						<h4 class="product-suite-box__headline"><?php echo $headline; ?></h4>
					<?php endif; ?>

					<?php if( $text ): ?>
						<p class="product-suite-box__text"><?php echo $text; ?></p>
					<?php endif; ?>

				</div>

			<?php endwhile; ?>

			</div>

			<?php endif; ?>

			<!-- The Image -->
			<?php

			$image = get_field('products_image');

			if( !empty($image) ): ?>

			<div class="products_image-wrap">
				<img src="<?php echo $image; ?>" alt="" />
			</div>

			<?php endif; ?>

		<!-- Powerful Insurance Tools -->
		<?php

		// check if the repeater field has rows of data
		if( have_rows('powerful_insurance_tools') ): ?>

			<h2 class="title--extra-large text--center"><?php echo get_field_object(powerful_insurance_tools)['label']; ?></h2>

			<div class="powerful-insurance-tools">
			<?php	// loop through the rows of data
				while( have_rows('powerful_insurance_tools') ): the_row();

			// vars
				$icon = get_sub_field('icon');
				$headline = get_sub_field('headline');
				$text = get_sub_field('text');
				$link = get_sub_field('link');

			?>
			<div class="insurance-tool">

				<a href="<?php echo $link; ?>" class="insurance-tool__link">

					<div class="insurance-tool__image-wrap">
						<img src="<?php echo $icon; ?>" alt="" />
					</div>

					<div class="insurance-tool__text-wrap">
						<?php if( $headline ): ?>
							<h4 class="insurance-tool__headline"><?php echo $headline; ?></h4>
						<?php endif; ?>

						<?php if( $text ): ?>
							<p class="insurance-tool__text"><?php echo $text; ?></p>
						<?php endif; ?>
					</div>

				</a>
			</div>

		<?php endwhile; ?>

		</div>

		<?php endif; ?>

		<!-- Download All Button -->
		<?php

		$file = get_field('download_all_btn');

		if( $file ): ?>

		<div class="button-row">
			<a href="<?php echo $file; ?>" class="button button--secondary">Download All</a>
		</div>

		<?php endif; ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
