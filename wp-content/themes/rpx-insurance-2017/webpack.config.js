const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
    entry: {
        src: './src/index.js'
    },
    output: {
        path: path.resolve(__dirname, './'),
        filename: '[name].js',
    },
    module: {
        rules: [{
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        'css-loader?importLoaders=1',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function() {
                                    return [
                                        require('autoprefixer')
                                    ];
                                }
                            }
                        },
                        'resolve-url-loader',
                        'sass-loader?sourceMap',

                    ]
                })
            },
            {
                test: /\.(eot|ttf|woff|woff2|svg|jpg|png|gif)$/,
                loader: 'file-loader',
                options: {
                    name: 'assets/[name].[ext]'
                }
            }
        ]
    },
    devtool: 'source-map',
    plugins: [
        new ExtractTextPlugin("style.css"),
    ]
}
