<?php
/**
 * Template part for displaying faqs
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

?>

<?php if( have_rows('faq_item') ): ?>

<div class="faq__wrap">

	<?php
	// loop through the rows of data
	while( have_rows('faq_item') ): the_row();

	// vars
	$headline = get_sub_field('headline');
	$text = get_sub_field('text');
	?>

	<div class="faq__item">

		<?php if( $headline ): ?>
			<a class="faq__headline" href="#"><?php echo $headline; ?></a>
		<?php endif; ?>

		<?php if( $text ): ?>
			<div class="faq__text">
				<?php echo $text; ?>
			</div>
		<?php endif; ?>

	</div><!-- .faq__item -->

	<?php endwhile; ?>

</div><!-- .faq__wrap -->

<?php endif; ?>
