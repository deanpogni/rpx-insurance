<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

get_header(); ?>

	<header class="entry-header">
		<div class="page-header-wrap">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if ( get_field('subheadline') ): ?>
			<div class="text--subheadline">
				<?php echo get_field('subheadline'); ?>
			</div>
			<?php endif; ?>

			<h3 class="field-label">Contact Information</h3>

			<form class="input-form" action="http://info.rpxcorp.com/l/37272/2017-04-11/fj7g3d" method="post">

				<select id="userAudiences" name="member_type" required data-invalid="" aria-invalid="true">
					<option value="">I am a/an</option>
					<option value="applicant">Applicant</option>
					<option value="broker">Broker</option>
					<option value="other">Other</option>
				</select>

				<input type="text" id="userFirstName" name="first_name" maxlength="40" placeholder="Your First Name" required>

				<input type="text" id="userLastName" name="last_name" maxlength="40" placeholder="Your Last Name" required>

				<input type="text" id="userCompany" name="company" maxlength="40" placeholder="Company" required>

				<input type="text" id="userPhone" name="phone" maxlength="20" placeholder="Phone" required>

				<input type="email" id="userEmail" name="email" maxlength="40" placeholder="Your E-mail" required>

				<div class="button-row">
					<button type="submit" class="button button--primary">Submit</button>
				</div>

			</form>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
