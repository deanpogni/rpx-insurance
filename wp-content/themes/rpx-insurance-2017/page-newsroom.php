<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

get_header(); ?>

	<header class="entry-header">
		<div class="page-header-wrap">

			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="103.875px" height="72.541px" viewBox="0 0 103.875 72.541" enable-background="new 0 0 103.875 72.541"
				 xml:space="preserve">
			<g>
				<g>
					<polygon fill="none" stroke="#FFFFFF" stroke-width="2.06" stroke-miterlimit="10" points="63.481,53.53 47.7,53.53
						29.663,70.214 29.438,53.53 1.03,53.53 1.03,11.698 63.481,11.698 		"/>
					<path fill="none" stroke="#FFFFFF" stroke-width="2.06" stroke-miterlimit="10" d="M69.179,45.03l20.5,8.5l0.174-13.135
						c7.906-4.025,12.992-10.307,12.992-17.365c0-12.15-15.072-22-33.666-22c-10.02,0-19.016,2.859-25.184,7.398"/>
				</g>
				<line fill="none" stroke="#FFFFFF" stroke-width="2.06" stroke-miterlimit="10" x1="9.507" y1="22.271" x2="55.007" y2="22.271"/>
				<line fill="none" stroke="#FFFFFF" stroke-width="2.06" stroke-miterlimit="10" x1="9.507" y1="29.024" x2="55.007" y2="29.024"/>
				<line fill="none" stroke="#FFFFFF" stroke-width="2.06" stroke-miterlimit="10" x1="9.507" y1="35.776" x2="55.007" y2="35.776"/>
				<line fill="none" stroke="#FFFFFF" stroke-width="2.06" stroke-miterlimit="10" x1="9.507" y1="42.528" x2="55.007" y2="42.528"/>
			</g>
			</svg>

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		</div>
	</header>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if( have_rows('news_articles') ): ?>

			<div class="news-articles">
				<h2 class="title--extra-large text--center">News Articles</h2>

				<div class="news-articles__items">

				<?php
				// loop through the rows of data
				while( have_rows('news_articles') ): the_row();

				// vars
				$link = get_sub_field('link');
				$category = get_sub_field('category');
				$headline = get_sub_field('headline');
				?>
				<div class="news-articles__item">
					<a href="<?php echo $link; ?>" target="_blank" class="news-articles__link">

					<?php if( $category ): ?>
						<h5 class="news-articles__link-category"><?php echo $category; ?></h5>
					<?php endif; ?>

					<?php if( $headline ): ?>
						<h3 class="news-articles__link-headline"><?php echo $headline; ?></h3>
					<?php endif; ?>

					</a>
				</div>

				<?php endwhile; ?>

				</div>
			</div>

			<?php endif; ?><!-- .news_articles -->

			<?php if( have_rows('press_releases') ): ?>

			<div class="press-releases">

				<h2 class="title--extra-large text--center">Press Releases</h2>

				<div class="press-releases__items">

				<?php
				// loop through the rows of data
				while( have_rows('press_releases') ): the_row();

				// vars
				$link = get_sub_field('link');
				$date = get_sub_field('date');
				$headline = get_sub_field('headline');
				?>
				<div class="press-releases__item">

					<a href="<?php echo $link; ?>" target="_blank" class="press-releases__link">

						<?php if( $date ): ?>
							<div class="press-releases__date">
								<?php echo $date; ?>
							</div>
						<?php endif; ?>

						<?php if( $headline ): ?>
							<h3 class="press-releases__link-headline"><?php echo $headline; ?></h3>
						<?php endif; ?>

					</a>

				</div>

				<?php endwhile; ?>

			</div>

			<?php endif; ?><!-- .news_articles -->

			<?php get_template_part( 'template-parts/learn-more' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
