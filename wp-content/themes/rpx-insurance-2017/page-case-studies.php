<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

get_header(); ?>

	<header class="entry-header">
		<div class="page-header-wrap">

			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="55.317px" height="79.565px" viewBox="0 0 55.317 79.565" enable-background="new 0 0 55.317 79.565" xml:space="preserve">
			<g>
				<polygon fill="none" stroke="#FFFFFF" stroke-width="2.06" stroke-miterlimit="10" points="50.824,27.659 53.954,22.428
					49.06,18.794 49.951,12.764 44.038,11.278 42.554,5.366 36.523,6.257 32.889,1.363 27.658,4.494 22.428,1.363 18.794,6.257
					12.764,5.366 11.279,11.278 5.365,12.764 6.258,18.794 1.363,22.428 4.494,27.659 1.363,32.89 6.258,36.523 5.365,42.554
					11.279,44.039 12.764,49.951 15.951,49.48 7.285,78.535 20.705,78.535 27.658,55.22 34.612,78.535 48.031,78.535 39.365,49.48
					42.554,49.951 44.038,44.039 49.951,42.554 49.06,36.523 53.954,32.89 	"/>
				<circle fill="none" stroke="#FFFFFF" stroke-width="2.06" stroke-miterlimit="10" cx="27.211" cy="28.094" r="16.813"/>
			</g>
			</svg>

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php if ( get_field('headline') ): ?>
			<div class="text--subheadline">
				<p><?php echo get_field('headline'); ?></p>
			</div>
			<?php endif; ?>

			<?php

			// check if the repeater field has rows of data
			if( have_rows('results') ): ?>

			<div class="case-study-results">

				<?php

				// loop through the rows of data
				while( have_rows('results') ): the_row();

				// vars
				$result = get_sub_field('result');
				$headline = get_sub_field('headline');
				$intro_text = get_sub_field('intro_text');
				$read_more_text = get_sub_field('read_more_text');

				?>

				<?php if( $headline ): ?>

				<div class="case-study-results__result">

					<?php if( $result ): ?>
						<h4 class="case-study-results__result-headline"><?php echo $result; ?></h4>
					<?php endif; ?>

					<?php if( $headline ): ?>
						<h4 class="case-study-results__headline"><?php echo $headline; ?></h4>
					<?php endif; ?>

					<?php if( $intro_text ): ?>
						<div class="case-study-results__intro-text"><?php echo $intro_text; ?></div>
					<?php endif; ?>

					<?php if( $read_more_text ): ?>
						<a href="#" class="case-study-results__read-more-btn" data-link="more">+ Read more</a>
						<div class="case-study-results__read-more"><?php echo $read_more_text; ?></div>
					<?php endif; ?>

				</div>

				<?php endif; ?>

			<?php endwhile; ?>

			<?php endif; ?>

			<?php get_template_part( 'template-parts/learn-more' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer();
