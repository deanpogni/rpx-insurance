<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

get_header(); ?>

	<header class="entry-header">
		<div class="page-header-wrap">

			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="62px" height="62px" viewBox="0 0 62 62" enable-background="new 0 0 62 62" xml:space="preserve">
			<g id="Page-1">
				<path fill="none" stroke="#FFFFFF" stroke-width="1.5" d="M51.579,60.777l9.021-8.494L46.131,37.23
					c2.023-3.533,3.182-7.626,3.182-11.99c0-13.342-10.816-24.156-24.157-24.156C11.816,1.084,1,11.898,1,25.24
					s10.816,24.157,24.156,24.157c4.523,0,8.756-1.243,12.375-3.406L51.579,60.777z M42.656,25.24c0,9.665-7.836,17.501-17.5,17.501
					c-9.664,0-17.5-7.836-17.5-17.501c0-9.666,7.836-17.5,17.5-17.5C34.82,7.74,42.656,15.574,42.656,25.24z"/>
			</g>
			</svg>

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php get_template_part( 'template-parts/content-faqs' ); ?>


			<h3 class="faq-more__headline text--center">Still have a question?</h3>
			<div class="button-row">
				<a class="button button--primary" href="/contact">Get in Touch</a>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
