<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

get_header(); ?>

	<header class="entry-header">
		<div class="page-header-wrap">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php

			$topContent = get_field('top_content_quote');
			$form = get_field('get_a_quote_form');
			$thankYou = get_field('form_thank_you');

			if( !empty($topContent) ): ?>

			<div class="text--subheadline">
				<p><?php echo $topContent; ?></p>
			</div>

			<?php endif; ?>

			<form action="http://info.rpxcorp.com/l/37272/2017-04-11/fj7gkv" method="post" class="input-form">

				<label class="field-label">Contact Information</label>

				<input type="text" id="userFirstName" name="first_name" maxlength="40" placeholder="Your First Name" required>

				<input type="text" id="userLastName" name="last_name" maxlength="40" placeholder="Your Last Name" required>

				<input type="text" id="userCompany" name="company" maxlength="40" placeholder="Company">

				<input type="text" id="userAddress" name="address" maxlength="40" placeholder="Address" required>

				<input type="text" id="userState" name="state" maxlength="40" placeholder="State / Province" required>

				<input type="text" id="userZipcode" name="zipcode" maxlength="20" placeholder="Zip Code / Postal Code" required>

				<input type="text" id="userCity" name="city" maxlength="40" placeholder="City / Town" required>

				<input type="email" id="userEmail" name="email" maxlength="40" placeholder="Your E-mail" required>

				<input type="text" id="userPhone" name="phone" maxlength="20" placeholder="Contact Number" required>

				<fieldset id="group1">
					<legend class="field-label">What are your annual revenues?</legend>
			        <div class="radio-group">
			        	<input type="radio" value="0-5m" name="user_annual_rev" id="user_annual_rev1"><label for="user_annual_rev1">$0 - $5M</label>
					</div>
					<div class="radio-group">
			        	<input type="radio" value="5m-20m" name="user_annual_rev" id="user_annual_rev2"><label for="user_annual_rev2">$5M - $20M</label>
					</div>
					<div class="radio-group">
			        	<input type="radio" value="20m-100m" name="user_annual_rev" id="user_annual_rev2"><label for="user_annual_rev2">$20M - $100M</label>
					</div>
					<div class="radio-group">
			        	<input type="radio" value="100m-plus" name="user_annual_rev" id="user_annual_rev3"><label for="user_annual_rev3">$100M+</label>
					</div>
				</fieldset>

				<fieldset id="group2">
					<legend class="field-label">Have you had any patent litigation cases to date?</legend>
					<div class="radio-group">
						<input type="radio" value="yes" name="user_cases" id="user_cases_yes"><label for="user_cases_yes">Yes</label>
					</div>
					<div class="radio-group">
			        	<input type="radio" value="no" name="user_cases" id="user_cases_no"><label for="user_cases_no">No</label>
					</div>
				</fieldset>

				<fieldset id="group2">
					<legend class="field-label">Have you received any assertion letters or invitations to license?</legend>
					<div class="radio-group">
						<input type="radio" value="yes" name="user_license" id="user_license_yes"><label for="user_license_yes">Yes</label>
					</div>
					<div class="radio-group">
			        	<input type="radio" value="no" name="user_license" id="user_license_no"><label for="user_license_no">No</label>
					</div>
				</fieldset>

				<label class="field-label">Are you currently working with an insurance broker? If so, please provide their name and brokerage.</label>
				<input type="text" id="other_brokerage" name="other_brokerage" maxlength="20" placeholder="Broker Name / Brokerage">

				<div class="button-row">
					<button type="submit" class="button button--primary">Submit</button>
				</div>

			</form>

			<?php get_template_part( 'template-parts/learn-more' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
