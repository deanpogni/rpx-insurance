<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rpx-insurance
 */

get_header(); ?>

	<header class="entry-header">
		<div class="page-header-wrap">

			<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="79.441px" height="73.956px" viewBox="0 0 79.441 73.956" enable-background="new 0 0 79.441 73.956" xml:space="preserve">
			<g>
				<polygon fill="none" stroke="#FFFFFF" stroke-width="2.0598" stroke-miterlimit="10" points="21.979,30.957 26.681,36.515
					7.87,44.21 38.651,55.753 71.144,43.354 51.905,36.942 58.745,30.957 78.411,38.225 78.411,57.891 38.651,72.854 1.03,58.318
					1.03,39.935 	"/>
				<polygon fill="none" stroke="#FFFFFF" stroke-width="2.0598" stroke-miterlimit="10" points="35.231,1.03 35.231,20.269
					21.124,20.269 39.721,39.507 57.89,20.269 44.638,20.269 44.638,1.03 	"/>
			</g>
			</svg>

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>
	</header>

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">

			<div class="text--subheadline">
				<?php echo get_field('text'); ?>
			</div>

			<?php

			// check if the repeater field has rows of data
			if( have_rows('broker_resources_boxes') ): ?>

			<div class="box-wrapper">

				<?php

				// loop through the rows of data
				while( have_rows('broker_resources_boxes') ): the_row();

				// vars
				$headline = get_sub_field('headline');
				$text = get_sub_field('text');
				$link = get_sub_field('link');
				$image = get_sub_field('image');

				?>

				<?php if( $link ): ?>

				<div class="box box--image-bottom">

					<a href="<?php echo $link; ?>">

					<div class="box__text-wrap">
						<?php if( $headline ): ?>
							<h4 class="box__headline"><?php echo $headline; ?></h4>
						<?php endif; ?>

						<?php if( $text ): ?>
							<p class="box__text"><?php echo $text; ?></p>
						<?php endif; ?>
					</div>

					<?php if( $image ): ?>
						<div class="box__image-wrap box__image-wrap--bottom">
							<img src="<?php echo $image; ?>" alt="" />
						</div>
					<?php endif; ?>

					</a>

				</div>

			<?php endif; ?>

			<?php endwhile; ?>

		</div>

	<?php endif; ?>

	<div class="powerful-insurance-tools">
		<div class="insurance-tool">
			<a href="/broker-rundown/" class="insurance-tool__link">
				<div class="insurance-tool__image-wrap">
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 width="44.107px" height="56.251px" viewBox="0 0 44.107 56.251" enable-background="new 0 0 44.107 56.251" xml:space="preserve">
					<g>
						<rect x="0.75" y="0.75" fill="none" stroke="#057DC1" stroke-width="1.5" stroke-miterlimit="10" width="42.607" height="54.751"/>
						<line fill="none" stroke="#057DC1" stroke-width="1.5" stroke-miterlimit="10" x1="8.996" y1="8.845" x2="34.996" y2="8.845"/>
						<line fill="none" stroke="#057DC1" stroke-width="1.5" stroke-miterlimit="10" x1="8.996" y1="16.345" x2="34.996" y2="16.345"/>
						<line fill="none" stroke="#057DC1" stroke-width="1.5" stroke-miterlimit="10" x1="8.996" y1="23.845" x2="34.996" y2="23.845"/>
						<line fill="none" stroke="#057DC1" stroke-width="1.5" stroke-miterlimit="10" x1="8.996" y1="31.345" x2="34.996" y2="31.345"/>
						<line fill="none" stroke="#057DC1" stroke-width="1.5" stroke-miterlimit="10" x1="8.996" y1="38.845" x2="34.996" y2="38.845"/>
						<line fill="none" stroke="#057DC1" stroke-width="1.5" stroke-miterlimit="10" x1="8.996" y1="46.512" x2="34.996" y2="46.512"/>
					</g>
					</svg>

				</div>
				<div class="insurance-tool__text-wrap">
					<h4 class="insurance-tool__headline">The Broker Rundown</h4>
					<p class="insurance-tool__text">Everything you need to know about your clients’ patent risk – and the insurance solutions available to protect them</p>
				</div>
			</a>
		</div>
	</div>

	<!-- Download All Button -->
	<?php

	$file = get_field('download_all_button');

	if( $file ): ?>

	<div class="button-row">
		<a href="<?php echo $file; ?>" class="button button--secondary">Download All</a>
	</div>

	<?php endif; ?>

	<?php get_template_part( 'template-parts/learn-more' ); ?>

	</main><!-- #main -->

</div><!-- #primary -->

<?php
get_footer();
